# VS Code example for NPP

This workspace provides a `settings.json` file that provides CMake with the location of CUDA on Arch Linux.

Open this directory as workspace in VS Code and configure with the CMake extension. The `clangd` extension is recommended for C++ support.