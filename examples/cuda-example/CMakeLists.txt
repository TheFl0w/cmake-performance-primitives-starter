cmake_minimum_required(VERSION 3.24)
project("IPP example project" LANGUAGES CXX CUDA)
find_package(CUDAToolkit REQUIRED)
add_executable(example example.cu)
target_compile_features(example PRIVATE cxx_std_17)
target_link_libraries(example PRIVATE 
    CUDA::cudart_static
    CUDA::nppc_static
)