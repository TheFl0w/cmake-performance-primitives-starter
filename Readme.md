# Using Performance Primitives with CMake
## Quick Start
Example projects that work out of the box with VS Code on Arch linux can be found in the `examples` directory. 
## Intel Performance Primitives (IPP)
1. Install the libraries. On Arch Linux the package
    ```bash
    $ pacman -S intel-oneapi-basekit
    ```
    will provide a full installation which is quite large. Alternatively,
    
    - `aur/intel-oneapi-ipp-static` and
    - `aur/intel-oneapi-ipp`

    can be used.
2. Decide between static or dynamic libraries before you import them.
   ```CMake
   set(IPP_SHARED OFF) # static linkage (default, recommended)
   set(IPP_SHARED ON)  # dynamic linkage
   ```
2. Import the libraries into your CMake project:
   ```CMake
   find_package(IPP REQUIRED)
   ```
   If CMake cannot find IPP, we need to provide the path to the CMake package via the variable `IPP_DIR`. The default installation path on Arch Linux and MacOS seems to be
   ```bash
   /opt/intel/oneapi/ipp/latest/lib/cmake/ipp
   ```
   In cross-platform projects this path should not be hardcoded into the `CMakeLists.txt`. The CMake extension of VS Code allows us to pass parameters to CMake via `cmake.configureSettings`. It is recommended to create a workspace settings file `${WORKSPACE}/.vscode/settings.json` and add
    ```json
    {
        "cmake.configureSettings": {
            "IPP_DIR": "/opt/intel/oneapi/ipp/latest/lib/cmake/ipp"
        }
    }
    ```

3. Add the dependencies to your project. The IPP package exports the following libraries.
    
    - `IPP::ipp_iw `
    - `IPP::ippcore`
    - `IPP::ippcc`
    - `IPP::ippdc` 
    - `IPP::ippch` 
    - `IPP::ippcv`
    - `IPP::ippe`
    - `IPP::ippi`
    - `IPP::ipps`
    - `IPP::ippvm`

    We can link against them as usual with `target_link_libraries`, e.g.
    ```CMake
    target_link_libraries(my_executable PRIVATE IPP::ippcore)
    ```
    and all include paths and compiler flags will get automatically resolved.
## NVIDIA Performance Primitives
1. Install the libraries. They are shipped with CUDA by default. The corresponding package on Arch Linux is installed via
   ```bash
   $ pacman -S cuda
   ```

2. Import the libraries into your CMake project:
   ```CMake
   find_package(CUDAToolkit REQUIRED)
   ```
   If CMake cannot find CUDA, we need to provide the path via the variable `CUDAToolkit_ROOT`. The default installation path on Arch Linux seems to be
   ```bash
   /opt/cuda
   ```
   In cross-platform projects this path should not be hardcoded into the `CMakeLists.txt`. The CMake extension of VS Code allows us to pass parameters to CMake via `cmake.configureSettings`. It is recommended to create a workspace settings file `${WORKSPACE}/.vscode/settings.json` and add
    ```json
    {
        "cmake.configureSettings": {
            "CUDAToolkit_ROOT": "/opt/cuda",
            "CMAKE_CUDA_COMPILER": "/opt/cuda/bin/nvcc"
        }
    }
    ```
3. Add the dependencies to your project. A list of all exported libraries is available at https://cmake.org/cmake/help/latest/module/FindCUDAToolkit.html.  Typically, we want to link against
    
    - the CUDA runtime: `CUDA::cudart_static` or `CUDA::cuda_rt` and
    - at least one NPP library, e.g. `CUDA::nppif_static` or `CUDA::nppif`.

    We can link against them as usual with `target_link_libraries`,
    ```CMake
    target_link_libraries(my_executable PRIVATE 
        CUDA::cudart_static 
        CUDA::nppif_static
    )
    ```
    and all include paths and compiler flags will get automatically resolved.